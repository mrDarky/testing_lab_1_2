import unittest
import fudge
from parameterized import parameterized
from exceptions.types import CriticalException, NotCriticalException, AnotherCriticalException, \
    TotallyNotCriticalException, UnknownException
from exceptions.tester import ExceptionTester
from exceptions.sender import AlmostRealSender
from exceptions.config import ConfigurationManager
from exceptions.handler import ExceptionHandler
from great_productor import global_senders_manufacturer, global_configs_manufacturer


class ConfigManagerStub(ConfigurationManager):

    def get_critical_exceptions(self):
        return (
            CriticalException,
            AnotherCriticalException
        )


class SenderStubAlwaysFalse(AlmostRealSender):
    def send(self, exception: Exception):
        self.errors_count += 1
        return False


class HandlerTest(unittest.TestCase):
    def setUp(self):
        global_senders_manufacturer.set_sender(SenderStubAlwaysFalse)
        global_configs_manufacturer.set_manager(ConfigManagerStub)

    def tearDown(self):
        global_configs_manufacturer.set_manager(ConfigurationManager)
        global_senders_manufacturer.set_sender(AlmostRealSender)

    @fudge.test
    def test_critical_exceptions_count_valid(self):
        config_manager = ConfigManagerStub()
        tester = ExceptionTester(config_manager)
        sender = (fudge.Fake('AlmostRealSender')
                  .expects('send')
                  .times_called(3)
                  )
        handler = ExceptionHandler(tester=tester, sender=sender)
        exceptions_to_test = (
            CriticalException(),
            CriticalException(),
            NotCriticalException(),
            AnotherCriticalException()
        )

        for e in exceptions_to_test:
            handler.handle_exception(e)

    def test_not_critical_exceptions_count_valid_another(self):
        handler = ExceptionHandler()
        exceptions_to_test = (
            NotCriticalException(),
            AnotherCriticalException(),
            UnknownException(),
            CriticalException(),
            CriticalException(),
            NotCriticalException()
        )
        expected_not_criticals_count = 3

        for e in exceptions_to_test:
            handler.handle_exception(e)
        result = handler.get_not_critical_exceptions_count()

        self.assertEqual(result, expected_not_criticals_count)

    def test_not_critical_exceptions_count_valid(self):
        handler = ExceptionHandler()
        exceptions_to_test = (
            NotCriticalException(),
            AnotherCriticalException(),
            NotCriticalException(),
            UnknownException(),
            CriticalException()
        )
        expected_not_criticals_count = 3

        for e in exceptions_to_test:
            handler.handle_exception(e)
        result = handler.get_not_critical_exceptions_count()

        self.assertEqual(result, expected_not_criticals_count)


class TesterTest(unittest.TestCase):
    def setUp(self):
        self._tester = ExceptionTester()
        self._tester.manager = ConfigManagerStub()

    @parameterized.expand([
        (CriticalException(), True),
        (AnotherCriticalException(), True),
        (NotCriticalException(), False),
        (TotallyNotCriticalException(), False),
        (UnknownException(), False)
    ])
    def test_many_exceptions_valid(self, exception, expected):
        result = self._tester.exception_is_critical(exception)

        self.assertEqual(result, expected)

    @unittest.expectedFailure
    def test_critical_exception_invalid(self):
        result = self._tester.exception_is_critical(AnotherCriticalException())

        self.assertFalse(result)
