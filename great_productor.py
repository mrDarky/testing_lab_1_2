from exceptions.config import ConfigurationManager
from exceptions.sender import AlmostRealSender


class ConfigFactory(object):
    def __init__(self):
        self.__custom_manager = None

    def create(self):
        if self.__custom_manager:
            return self.__custom_manager()
        return ConfigurationManager()

    def set_manager(self, manager_class):
        self.__custom_manager = manager_class


class SenderFactory(object):
    def __init__(self):
        self.__custom_sender = None

    def create(self):
        if self.__custom_sender:
            return self.__custom_sender()
        return AlmostRealSender()

    def set_sender(self, sender_class):
        self.__custom_sender = sender_class


global_configs_manufacturer = ConfigFactory()
global_senders_manufacturer = SenderFactory()
