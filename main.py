from exceptions.handler import ExceptionHandler
from exceptions.tester import ExceptionTester
from exceptions.types import CriticalException, NotCriticalException, AnotherCriticalException, UnknownException


if __name__ == '__main__':
    handler = ExceptionHandler()
    tester = ExceptionTester()

    exceptions = (
        NotCriticalException(),
        CriticalException(),
        AnotherCriticalException(),
        NotCriticalException(),
        UnknownException()
    )

    for e in exceptions:
        print('{} is {} exception'.format(
            e.__class__.__name__,
            'a critical' if tester.exception_is_critical(e) else 'not a critical'
        ))
        handler.handle_exception(e)

    print('Total critical exceptions count: {}'.format(
        handler.get_critical_exceptions_count()
    ))
