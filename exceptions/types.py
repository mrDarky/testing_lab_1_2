class NotCriticalException(Exception):
    pass


class CriticalException(Exception):
    pass


class AnotherCriticalException(Exception):
    pass


class TotallyNotCriticalException(Exception):
    pass


class UnknownException(Exception):
    pass
