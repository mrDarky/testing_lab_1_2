from random import random


class AlmostRealSender:
    def __init__(self):
        self.errors_count = 0

    def send(self, exception: Exception):
        if random() < 0.8:
            return True
        self.errors_count += 1
        return False
