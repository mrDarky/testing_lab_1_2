from exceptions.types import *


class ConfigurationManager(object):

    def __init__(self):
        self.critical_exceptions = []

    def get_critical_exceptions(self):
        if len(self.critical_exceptions) == 0 and isinstance(self.critical_exceptions, list):
            with(open('exceptions/exception_classes')) as f:
                s = f.read()
                e_str_list = s.split()
                for e_str in e_str_list:
                    self.critical_exceptions.append(eval(e_str))
                self.critical_exceptions = tuple(self.critical_exceptions)

        return self.critical_exceptions
