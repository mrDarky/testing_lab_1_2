from great_productor import global_configs_manufacturer


class ExceptionTester(object):

    def __init__(self, manager=None):
        if manager:
            self.manager = manager
        else:
            self.manager = global_configs_manufacturer.create()

    def exception_is_critical(self, exception):
        return isinstance(exception, self.manager.get_critical_exceptions())
