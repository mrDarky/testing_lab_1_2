from exceptions.tester import ExceptionTester
from great_productor import global_senders_manufacturer


class ExceptionHandler(object):

    def __init__(self, tester=None, sender=None):
        self.__critical_exceptions_counter = 0
        self.__not_critical_exceptions_counter = 0
        if tester:
            self.tester = tester
        else:
            self.tester = ExceptionTester()
        if sender:
            self.sender = sender
        else:
            self.sender = global_senders_manufacturer.create()

    def get_critical_exceptions_count(self):
        return self.__critical_exceptions_counter

    def get_not_critical_exceptions_count(self):
        return self.__not_critical_exceptions_counter

    def handle_exception(self, exception: Exception):
        if self.tester.exception_is_critical(exception):
            self.__critical_exceptions_counter += 1
            self.sender.send(exception)
        else:
            self.__not_critical_exceptions_counter += 1
